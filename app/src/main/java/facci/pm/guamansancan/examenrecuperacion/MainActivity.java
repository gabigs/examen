package facci.pm.guamansancan.examenrecuperacion;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import facci.pm.guamansancan.examenrecuperacion.Adapter.ModelAdapter;
import facci.pm.guamansancan.examenrecuperacion.Model.Model1;

public class MainActivity extends AppCompatActivity{

    private RecyclerView recyclerView;
    private ArrayList<Model1> model1ArrayList;
    private ModelAdapter modelAdapter;
    private static final String URL_E = "http://10.1.15.127:3005/api/estudiantes/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView)findViewById(R.id.RecyclerModel1);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        model1ArrayList = new ArrayList<>();
        modelAdapter = new ModelAdapter(model1ArrayList);
        Llenar();
    }

    private void Llenar() {

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_E, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    Log.e("ARRAY", jsonArray.toString());
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        Log.e("JSON", jsonObject.toString());
                        Model1 model1 = new Model1();
                        model1.setId(jsonObject.getString("id"));
                        model1.setNombres(jsonObject.getString("nombres"));
                        model1.setApellidos(jsonObject.getString("apellidos"));
                        model1.setParcialuno(jsonObject.getString("parcial_uno"));
                        model1.setParcialdos(jsonObject.getString("parcial_dos"));
                        model1.setImagen(jsonObject.getString("imagen"));
                        model1ArrayList.add(model1);
                    }
                    recyclerView.setAdapter(modelAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, "No se pudo hacer la petición", Toast.LENGTH_SHORT).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);

    }
}



