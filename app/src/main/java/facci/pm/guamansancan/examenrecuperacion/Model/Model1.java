package facci.pm.guamansancan.examenrecuperacion.Model;

public class Model1 {

    private String id, nombres, apellidos, parcialuno, parcialdos, imagen;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getParcialuno() {
        return parcialuno;
    }

    public void setParcialuno(String parcialuno) {
        this.parcialuno = parcialuno;
    }

    public String getParcialdos() {
        return parcialdos;
    }

    public void setParcialdos(String parcialdos) {
        this.parcialdos = parcialdos;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}
