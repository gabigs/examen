package facci.pm.guamansancan.examenrecuperacion.Model;

public class Model2 {

    private String descripcion, parcialuno, parcialdos, imagen;

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getParcialuno() {
        return parcialuno;
    }

    public void setParcialuno(String parcialuno) {
        this.parcialuno = parcialuno;
    }

    public String getParcialdos() {
        return parcialdos;
    }

    public void setParcialdos(String parcialdos) {
        this.parcialdos = parcialdos;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
}
