package facci.pm.guamansancan.examenrecuperacion.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.ColorSpace;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import facci.pm.guamansancan.examenrecuperacion.Main2Activity;
import facci.pm.guamansancan.examenrecuperacion.Model.Model1;
import facci.pm.guamansancan.examenrecuperacion.R;

public class ModelAdapter extends RecyclerView.Adapter<ModelAdapter.MyViewHolder> {

    private ArrayList<Model1> modeloArrayList;

    public ModelAdapter(ArrayList<Model1> modeloArrayList) {
        this.modeloArrayList = modeloArrayList;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item1,viewGroup,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        final Model1 model1 =modeloArrayList.get(i);

        Picasso.get().load(model1.getImagen()).into(myViewHolder.imagen);
        myViewHolder.id.setText("Id:" + model1.getId());
        myViewHolder.nombres.setText("Nombres: "+model1.getNombres());
        myViewHolder.apellidos.setText("Apellidos: "+ model1.getApellidos());
        myViewHolder.parcialuno.setText("Parcial Uno: "+ model1.getParcialuno());
        myViewHolder.parcialdos.setText("Parcial Dos: "+ model1.getParcialdos());


        myViewHolder.views.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = v.getContext();
                Intent intent = new Intent(context, Main2Activity.class);
                intent.putExtra("id", model1.getId());
                context.startActivity(intent);
            }
        });



    }

    @Override
    public int getItemCount() {return modeloArrayList.size(); }


    public static class MyViewHolder extends RecyclerView.ViewHolder{

    private ImageView imagen;
    private TextView id, nombres, apellidos, parcialuno, parcialdos;
    private View views;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            views = itemView;
            id = (TextView)views.findViewById(R.id.id);
            imagen = (ImageView)views.findViewById(R.id.imagen);
            nombres = (TextView)views.findViewById(R.id.nombres);
            apellidos = (TextView)views.findViewById(R.id.apellidos);
            parcialuno = (TextView)views.findViewById(R.id.parcialuno);
            parcialdos = (TextView) views.findViewById(R.id.parcialdos);
        }
    }

    }






